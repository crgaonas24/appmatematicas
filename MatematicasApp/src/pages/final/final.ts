import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the FinalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-final',
  templateUrl: 'final.html',
})
export class FinalPage {
  myTracks: any[];
  allTracks: any[];
  userObject;
  nombre:string;
  score:number;
  minutos:number;
  segundos: number;
  correcta:number;
  incorrecta:number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.userObject = this.navParams.data;
    this.nombre = navParams.get('nombre');
    this.score = navParams.get('score');
    this.segundos = navParams.get('segundos');
    this.correcta = navParams.get('correcta');
    this.incorrecta = navParams.get('incorrecta');
    this.minutos = navParams.get("minutos");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinalPage');
  }
  jugarOtra(){
    this.navCtrl.setRoot('CategoriaPage', { nombre:this.nombre});
  }

 
}  


