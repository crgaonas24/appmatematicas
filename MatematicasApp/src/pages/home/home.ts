import { Component } from '@angular/core';
import { NavController,NavParams} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

//import { SegundaPage }from '../segunda/segunda';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'

})
export class HomePage {
 /* @ViewChild(Nav) nav: Nav;
  pages: Array<{tittle: string, component: string}>;*/
 // rootPage ='ConfiguracionPage'; 
  myForm: FormGroup;
  nombre: string;
  //segundaPage = "SegundaPage";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder   
    //public alert: AlertController

  ) {
  /*  this.pages = [
    {  tittle: 'Configuracion', component: 'ConfiguracionPage'},
    {  tittle: 'Contactos', component: 'ContactosPage' }
  ];*/
    
    this.myForm = this.createMyForm();

  }
 /* openPage(page){
    this.nav.setRoot(page.component)
  }*/
  
  saveData1() {
    console.log(this.nombre);
   // alert(this.nombre);
  }
  
  private createMyForm() {
    return this.formBuilder.group({
      name: ['', Validators.required]

    });
  }

  nextPage(){

    this.navCtrl.setRoot("CategoriaPage", {nombre:this.nombre});
    
  }







}

