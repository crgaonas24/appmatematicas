import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
//import { ProveedorProvider } from '../../providers/proveedor/proveedor';


/**
 * Generated class for the SegundaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-segunda',
  templateUrl: 'segunda.html',
})
export class SegundaPage {

  minutos: number;
  segundos: number;
  nombre = '';
  operacion: string='';
  userObject;
  nivel: number;
  bandera = false;
  controlCorrecta = false;
  controlIncorrecta = false;
  controlAsignación: number = 1;
  score: number = 0;
  intento: number = 5;
  num: number;
  num1: number;
  num2: number;
  numSum: number;
  resp: number;
  signo: String;
  bnt1: number;
  bnt2: number;
  bnt3: number;
  bnt4: number;
  mensaje: String;
 correcta:number= 0;
 incorrecta:number= 0;
 boton = document.getElementById("boton");
 v = document.getElementsByTagName("audio")[0];
 sound = false;
  innerHTML: string;
 
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
    
    //public proveedor: ProveedorProvider
  ) {
    this.userObject = this.navParams.data;
    this.nombre = navParams.get('nombre');
    this.operacion = navParams.get('operacion');
    this.nivel = navParams.get('nivel');
     this.boton = document.getElementById("boton");
    //this.asignacionNivelnum();
    this.num1= this.asignacionNivelnum();
    this.num2= this.asignacionNivelnum();
    this.resetTimer();
    this.asignacionLevelBnt();
   //this.letrasSleatorias();
   // this.operMath(this.num1, this.num2); // operaciones de un solo signo
   // this.OperacionLogica(this.num1, this.num2)
    setInterval(() => this.tick(), 1000);

  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad SegundaPage');
    console.log(this.userObject);
  }
  resetTimer() {
    this.minutos = 0;
    this.segundos = 0;

  }

  
  tick() {

    if(++this.segundos==59){
    this.minutos = this.minutos+1;
    this.segundos=0;
    }
    /*if (--this.segundos < 0) {
      this.segundos = 0;
      if (--this.minutos < 0) {
        this.minutos = 0;
        this.segundos = 30;
      }
    }*/
  }



  comprobarNumeroSumaBotones1() {
    //  this.numSum = this.OperacionLogica(this.num1,this.num2);
    if ((this.bnt1) == this.numSum) {
      this.controlCorrecta = true;
      this.score = this.score + 150;
      this.correcta = this.correcta+1;
    } else {
      this.controlIncorrecta = true;
      this.incorrecta ++;
    }

    this.intentos();
    this.bandera = true;
    //this.reinicia1();

  }

  comprobarNumeroSumaBotones2() {

    // this.numSum = this.OperacionLogica(this.num1,this.num2);

    if ((this.bnt2) == this.numSum) {
      this.controlCorrecta = true;
      this.score = this.score + 150;
      this.correcta = this.correcta+1;
    } else {
      this.controlIncorrecta = true;
      this.incorrecta ++;
    }
    this.intentos();
    this.bandera = true;
    //this.reinicia1();
  }

  comprobarNumeroSumaBotones3() {

    //this.numSum = this.OperacionLogica(this.num1,this.num2);
    if ((this.bnt3) == this.numSum) {
      this.controlCorrecta = true;
      this.score = this.score + 150;
      this.correcta = this.correcta+1;
    } else {
      this.controlIncorrecta = true;
      this.incorrecta ++;
    }
    this.intentos();
    this.bandera = true;
    //this.reinicia1();
  }

  comprobarNumeroSumaBotones4() {
    // this.numSum = this.OperacionLogica(this.num1,this.num2);
    if ((this.bnt4) == this.numSum) {
      this.controlCorrecta = true;
      this.score = this.score + 150;
      this.correcta = this.correcta+1;
    } else {
      this.controlIncorrecta = true;
      this.incorrecta ++;
    }
    this.intentos();
    this.bandera = true;
    // this.reinicia1();
  }

  numAleatorio(a:number, b:number) {
    return Math.round(Math.random() * (b - a) + (a));
  }

  asignacionNivelnum(){
    var result:number;
    if(this.nivel==1){
     result=this.numAleatorio(1,10);
    }else if(this.nivel==2){
      result=this.numAleatorio(10,20);
    }else if(this.nivel==3){
      result=this.numAleatorio(20,40); 
    }else if(this.nivel==4){
      result=this.numAleatorio(50,99);
    }
    return result;
  }

  letrasSleatorias() {
    var aLetras = new Array('+', '-', 'x');
    var cLetra = aLetras[Math.floor(Math.random() * aLetras.length)];
    return cLetra;

  }

  OperacionLogica(a, b) {
    var result: number;
    if (this.signo == "+") {
      result = (parseInt(a) + parseInt(b));
    } else if (this.signo == "-") {
      result = (parseInt(a) - parseInt(b));
    } else {
      result = (parseInt(a) * parseInt(b));
    }
    return result;
  }

  

  operMath(a, b) {
    var result: number;
    if (this.operacion == "suma") {
      this.signo = "+";
      result = (parseInt(a) + parseInt(b));
    } 
    else if (this.operacion == "resta") {
      this.signo = "-";
      result = (parseInt(a) - parseInt(b));
    } 
    else if (this.operacion == "multiplicacion") {
      this.signo = "x";
      result = (parseInt(a) * parseInt(b));
    } 
     else if (this.operacion == "mixta") {
      this.signo = this.letrasSleatorias();
      result = this.OperacionLogica(this.num1, this.num2);
     // alert(this.signo)
    }
    return result;
  }

  asignacionLevelBnt(){
    if(this.nivel==1){
      this.asignacionN1();
    }else if(this.nivel==2){
      this.asignacionN2();
    }else if(this.nivel==3){
      this.asignacionN3();
    }else if(this.nivel==4){
      this.asignacionN4();
    }
  }
  asignacionN1() {
    this.numSum = this.operMath(this.num1, this.num2);
    if (this.controlAsignación == 1) {
      this.bnt1 = this.numSum;
      this.bnt2 = this.numSum+1;
      this.bnt3 = this.numSum+2;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 2) {
      this.bnt1 = this.numSum+1;
      this.bnt2 = this.numSum;
      this.bnt3 = this.numSum+2;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 3) {
      this.bnt1 = this.numSum+1;
      this.bnt2 = this.numSum+2;
      this.bnt3 = this.numSum;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 4) {
      this.bnt1 = this.numSum+1;
      this.bnt2 = this.numSum+2;
      this.bnt3 = this.numSum-1;
      this.bnt4 = this.numSum;
      this.controlAsignación = 1;
    }

  }

  asignacionN2() {
    this.numSum = this.operMath(this.num1, this.num2);
    if (this.controlAsignación == 1) {
      this.bnt1 = this.numSum;
      this.bnt2 = this.numSum+1;
      this.bnt3 = this.numSum+2;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 2) {
      this.bnt1 = this.numSum+1;
      this.bnt2 = this.numSum;
      this.bnt3 = this.numSum+2;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 3) {
      this.bnt1 = this.numSum+1;
      this.bnt2 = this.numSum+2;
      this.bnt3 = this.numSum;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 4) {
      this.bnt1 = this.numSum+1;
      this.bnt2 = this.numSum+2;
      this.bnt3 = this.numSum-1;
      this.bnt4 = this.numSum;
      this.controlAsignación = 1;
    }

  }

  asignacionN3() {
    this.numSum = this.operMath(this.num1, this.num2);
    if (this.controlAsignación == 1) {
      this.bnt1 = this.numSum;
      this.bnt2 = this.numSum+1;
      this.bnt3 = this.numSum+2;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 2) {
      this.bnt1 = this.numSum+1;
      this.bnt2 = this.numSum;
      this.bnt3 = this.numSum+2;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 3) {
      this.bnt1 = this.numSum+1;
      this.bnt2 = this.numSum+2;
      this.bnt3 = this.numSum;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 4) {
      this.bnt1 = this.numSum+1;
      this.bnt2 = this.numSum+2;
      this.bnt3 = this.numSum-1;
      this.bnt4 = this.numSum;
      this.controlAsignación = 1;
    }

  }

  asignacionN4() {
    this.numSum = this.operMath(this.num1, this.num2);
    if (this.controlAsignación == 1) {
      this.bnt1 = this.numSum;
      this.bnt2 = this.numSum-2;
      this.bnt3 = this.numSum+1;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 2) {
      this.bnt1 = this.numSum+2;
      this.bnt2 = this.numSum;
      this.bnt3 = this.numSum;-1;
      this.bnt4 = this.numSum+3;
      this.controlAsignación++
    } else if (this.controlAsignación == 3) {
      this.bnt1 = this.numSum-2;
      this.bnt2 = this.numSum+3;
      this.bnt3 = this.numSum;
      this.bnt4 = this.numSum-1;
      this.controlAsignación++
    } else if (this.controlAsignación == 4) {
      this.bnt1 = this.numSum-1;
      this.bnt2 = this.numSum+2;
      this.bnt3 = this.numSum+3;
      this.bnt4 = this.numSum;
      this.controlAsignación = 1;
    }

  }

  intentos() {
    if (this.intento == 0) {
      alert("fin del juego");
    } else {
      this.intento--;
    }
  }
  reinicia1() {
    // reiniciamos las variables
    //this.num1 = this.numAleatorio(50, 75);
    //this.num2 = this.numAleatorio(1, 20);
    this.num1= this.asignacionNivelnum();
    this.num2= this.asignacionNivelnum();
    //this.signo = this.letrasSleatorias();
    //this.bnt2 = this.OperacionLogica(this.num1,this.num2);
    this.bandera = false;
    this.controlCorrecta = false;
    this.controlIncorrecta = false;
    this.asignacionLevelBnt();
    // alert(this.controlAsignación);
  }

  finalizar(){
    this.navCtrl.setRoot("FinalPage", {nombre:this.nombre,score:this.score,segundos:this.segundos,minutos:this.minutos,correcta:this.correcta, incorrecta:this.incorrecta});
  }




}


