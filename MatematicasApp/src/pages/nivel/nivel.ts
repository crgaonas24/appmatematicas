import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NivelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nivel',
  templateUrl: 'nivel.html',
})
export class NivelPage {
nombre: '';
operacion:'';
nivel:number;
userObject;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
   this.userObject= this.navParams.data;
    this.nombre=navParams.get('nombre');
    this.operacion=navParams.get('operacion');
    //this.operacion = this.navParams.data.get('operacion');
   // this.nombre=navParams.get('operacion');
    //this.userObject = this.navParams.data;
   // this.nombre  = this.userObject.nombreEnv;
    //this.operacion  = this.userObject.operacion;
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad NivelPage');
    console.log(this.userObject);
  }

  bntLevel1(){
    this.nivel=1;
    this.navCtrl.push("SegundaPage",{nombre:this.nombre, operacion:this.operacion, nivel:this.nivel});
  }

  bntLevel2(){
    this.nivel=2;
    this.navCtrl.push("SegundaPage",{nombre:this.nombre, operacion:this.operacion, nivel:this.nivel});
  }

  bntLevel3(){
    this.nivel=3;
    this.navCtrl.push("SegundaPage",{nombre:this.nombre, operacion:this.operacion, nivel:this.nivel});
  }

  bntLevel4(){
    this.nivel=4;
    this.navCtrl.push("SegundaPage",{nombre:this.nombre, operacion:this.operacion, nivel:this.nivel});
  }

}
