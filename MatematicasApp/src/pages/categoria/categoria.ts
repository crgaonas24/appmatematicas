import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { NivelPage } from '../nivel/nivel';

/**
 * Generated class for the CategoriaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categoria',
  templateUrl: 'categoria.html',
})
export class CategoriaPage {
nombre ='';
nombrell=this.nombre;
operacion:string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.nombre = navParams.get('nombre');
  }

  public userObject = {
    nombreEnv:this.nombrell,
    //operacion:'suma'
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriaPage');
  }

  bntSuma(){
  this.operacion="suma";
  this.navCtrl.push("NivelPage",{nombre:this.nombre,operacion:this.operacion});
  }
  bntResta(){
  this.operacion="resta";
  this.navCtrl.push("NivelPage",{nombre:this.nombre,operacion:this.operacion});
  }
  bntMultiplicacion(){
  this.operacion="multiplicacion";
  this.navCtrl.push("NivelPage",{nombre:this.nombre,operacion:this.operacion});
  }
  bntMixta(){
  this.operacion="mixta";
  this.navCtrl.push("NivelPage",{nombre:this.nombre,operacion:this.operacion});
  }

}
